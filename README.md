Script for yocto integration
=============================================

This repository is not meant to be cloned directly.  Please clone
https://gitlab.com/alexlargacha/yocto-manifests instead.

When initialized through the manifest, this repository is cloned at the root of
the source tree setup, and this is the holding ground for smartzynq environment
specific yocto setup and automation scripts.
